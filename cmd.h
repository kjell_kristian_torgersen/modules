#ifndef C3A730C0_06B6_43F4_BA43_25E7936F361B
#define C3A730C0_06B6_43F4_BA43_25E7936F361B

typedef struct cmd_ctx {
	int (*printf)(const char *fmt, ...);
} cmd_ctx_t;

typedef int (*cmd_callback_t)(cmd_ctx_t *ctx, const char *args);

typedef struct cmd {
	char	     *cmd;
	char         *help;
	cmd_callback_t callback;
} cmd_t;

int cmd_parse(const char *cmd, cmd_ctx_t *ctx, const cmd_t *cmds, int ncmds);

#endif /* C3A730C0_06B6_43F4_BA43_25E7936F361B */
