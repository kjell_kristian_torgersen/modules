#ifndef AB1CA45D_C451_4CCD_B6C6_4D5746FC75F2
#define AB1CA45D_C451_4CCD_B6C6_4D5746FC75F2

#include <stdint.h>

typedef struct linbuf {
	uint8_t *storage;
	uint16_t storage_size;
	uint16_t used_bytes;
} linbuf_t;

void  linbuf_init(linbuf_t *this, void *storage, int storage_size);
int   linbuf_write(linbuf_t *this, const void *data, int count);
void  linbuf_clear(linbuf_t *this);

/** \brief Erase count bytes. */
void  linbuf_erase(linbuf_t *this, int count);
void *linbuf_get(linbuf_t *this);
int   linbuf_len(linbuf_t *this);
int   linbuf_capacity(linbuf_t *this);
int   linbuf_free_space(linbuf_t *this);

#endif /* AB1CA45D_C451_4CCD_B6C6_4D5746FC75F2 */
