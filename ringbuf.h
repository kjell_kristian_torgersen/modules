#ifndef AD3F228C_A689_47ED_92E6_A2F0968094D7
#define AD3F228C_A689_47ED_92E6_A2F0968094D7

#include <stdint.h>

typedef struct ringbuf {
	uint8_t *storage;
	uint16_t read;
	uint16_t write;
	uint16_t storage_size;
} ringbuf_t;

void ringbuf_init(ringbuf_t *this, void *storage, int storage_size);
int  ringbuf_write(ringbuf_t *this, const void *buf, int count);
int  ringbuf_read(ringbuf_t *this, void *buf, int count);
int  ringbuf_len(ringbuf_t *this);
int  ringbuf_free_space(ringbuf_t *this);

#endif /* AD3F228C_A689_47ED_92E6_A2F0968094D7 */
