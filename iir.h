#ifndef A7C537DA_8CC5_466A_AEB7_F91407F9F71B
#define A7C537DA_8CC5_466A_AEB7_F91407F9F71B

typedef struct iir {
    int order;
    float strenght;
    float * y;
} iir_t;

void iir_init(iir_t * this, int order, float strenght, float * storage);
float iir_filter(iir_t * this, float x);


#endif /* A7C537DA_8CC5_466A_AEB7_F91407F9F71B */
