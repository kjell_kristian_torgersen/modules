#include <string.h>

#include "../cmd.h"

int cmd_parse(const char *cmd, cmd_ctx_t *ctx, const cmd_t *cmds, int ncmds)
{
	for (int i = 0; i < ncmds; i++) {
		int n = strlen(cmds[i].cmd);
		if (strncmp(cmd, cmds[i].cmd, n) == 0) {
			if (cmd[n] == '\0') {
				return cmds[i].callback(ctx, NULL);
			} else {
				return cmds[i].callback(ctx, &cmd[n + 1]);
			}
		}
	}
	return -1;
}
