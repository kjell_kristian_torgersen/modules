#include <string.h>

#include "../linbuf.h"

#define MIN(a, b) (((a) < (b)) ? (a) : (b))
#define MAX(a, b) (((a) > (b)) ? (a) : (b))

void linbuf_init(linbuf_t *this, void *storage, int storage_size)
{
	this->storage	   = storage;
	this->storage_size = storage_size;
	this->used_bytes   = 0;
}

int linbuf_write(linbuf_t *this, const void *data, int count)
{
	int n = MIN(linbuf_free_space(this), count);
	if (n) {
		memcpy(&this->storage[this->used_bytes], data, n);
		this->used_bytes += n;
	}
	return n;
}

void linbuf_clear(linbuf_t *this) { this->used_bytes = 0; }

void linbuf_erase(linbuf_t *this, int count)
{
	if (count >= this->used_bytes) {
		this->used_bytes = 0;
	} else {
		this->used_bytes -= count;
	}
}

void *linbuf_get(linbuf_t *this) { return this->storage; }

int linbuf_len(linbuf_t *this) { return this->used_bytes; }

int linbuf_capacity(linbuf_t *this) { return this->storage_size; }

int linbuf_free_space(linbuf_t *this) { return this->storage_size - this->used_bytes; }
