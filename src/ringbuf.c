#include "../ringbuf.h"

void ringbuf_init(ringbuf_t *this, void *storage, int storage_size)
{
	if (this) {
		this->storage	   = storage;
		this->read	   = 0;
		this->write	   = 0;
		this->storage_size = storage_size;
	}
}

int ringbuf_write(ringbuf_t *this, const void *buf, int count)
{
	const uint8_t *data    = buf;
	int	       written = 0;
	while (this->read != ((this->write + 1) % this->storage_size)) {
		this->storage[this->write] = data[written];
		this->write++;
		if (this->write == this->storage_size) {
			this->write = 0;
		}
		written++;
		if (written == count) {
			break;
		}
	}
	return written;
}

int ringbuf_read(ringbuf_t *this, void *buf, int count)
{
	uint8_t *data	    = buf;
	int	 bytes_read = 0;
	while (this->read != this->write) {
		data[bytes_read++] = this->storage[this->read];
		this->read++;
		if (this->read == this->storage_size) {
			this->read = 0;
		}
		/*if (this->read == this->write) {
			break;
		}*/
		if (bytes_read == count) {
			break;
		}
	}
	return bytes_read;
}

int ringbuf_len(ringbuf_t *this) { return (this->write - this->read) % this->storage_size; }

int ringbuf_free_space(ringbuf_t *this) { return this->storage_size - ringbuf_len(this) - 1; }