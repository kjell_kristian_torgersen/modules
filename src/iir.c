#include "../iir.h"

void iir_init(iir_t * this, int order, float strenght, float * storage) 
{
    this->order = order;
    this->strenght = strenght;
    this->y = storage;
    for(int i = 0; i < this->strenght; i++) {
        this->y[i] = 0.0f;
    }
}

float iir_filter(iir_t * this, float x) 
{
    this->y[0] = this->y[0] + ((x-this->y[0])*this->strenght);
    for(int i = 1; i < this->order; i++) {
        this->y[i] = this->y[i] + ((this->y[i-1]-this->y[i])*this->strenght);
    }
    return this->y[this->order - 1];
}