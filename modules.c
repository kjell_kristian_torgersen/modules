#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include <string.h>

#include "cmd.h"
#include "linbuf.h"
#include "ringbuf.h"

static int failed = 0, tests = 0;

static uint8_t buf[16];

#define ASSERT(x)                                                                                                                                                                                                                                                                      \
	{                                                                                                                                                                                                                                                                              \
		tests++;                                                                                                                                                                                                                                                               \
		if (!(x)) {                                                                                                                                                                                                                                                            \
			fprintf(stderr, "Assertion %s failed!\n", #x);                                                                                                                                                                                                                 \
			failed++;                                                                                                                                                                                                                                                      \
		}                                                                                                                                                                                                                                                                      \
	}

bool array_cmp(const void *aa, const void *bb, int count)
{
	const uint8_t *a = aa;
	const uint8_t *b = bb;
	for (int i = 0; i < count; i++) {
		if (a[i] != b[i]) {
			return false;
		}
	}
	return true;
}

void test_linbuf(void)
{
	linbuf_t lb;
	char	 storage[8];
	linbuf_init(&lb, storage, sizeof(storage));

	// Verify initialization is OK
	ASSERT(lb.storage == (void *)storage);
	ASSERT(lb.storage_size == sizeof(storage));
	ASSERT(lb.used_bytes == 0);

	// Verify a typical write
	ASSERT(linbuf_write(&lb, "hello", 5) == 5);
	ASSERT(linbuf_len(&lb) == 5);
	ASSERT(linbuf_capacity(&lb) == sizeof(storage));
	ASSERT(linbuf_free_space(&lb) == sizeof(storage) - 5);
	ASSERT(array_cmp(linbuf_get(&lb), "hello", 5));

	// Verify clearing
	linbuf_clear(&lb);
	ASSERT(linbuf_len(&lb) == 0);
	ASSERT(linbuf_capacity(&lb) == sizeof(storage));
	ASSERT(linbuf_free_space(&lb) == sizeof(storage));

	// Verify that writing to many bytes at once is gracefully handled
	ASSERT(linbuf_write(&lb, "0123456789", 10) == sizeof(storage));
	ASSERT(linbuf_len(&lb) == sizeof(storage));
	ASSERT(linbuf_capacity(&lb) == sizeof(storage));
	ASSERT(linbuf_free_space(&lb) == 0);
	ASSERT(array_cmp(linbuf_get(&lb), "01234567", 8));
}

void test_ringbuf(void)
{
	ringbuf_t rb;
	uint8_t	  tmp[4]  = {1, 2, 3, 4};
	uint8_t	  tmp2[4] = {5, 6, 7, 8};
	uint8_t	  big[32] = {0};
	ringbuf_init(&rb, buf, sizeof(buf));
	ASSERT(rb.storage == buf);
	ASSERT(rb.read == 0);
	ASSERT(rb.write == 0);
	ASSERT(rb.storage_size == sizeof(buf));

	ASSERT(ringbuf_len(&rb) == 0);
	ASSERT(ringbuf_free_space(&rb) == (sizeof(buf) - 1));
	ASSERT(ringbuf_read(&rb, tmp2, sizeof(tmp2)) == 0);
	ASSERT(tmp2[0] == 5);
	ASSERT(ringbuf_len(&rb) == 0);
	ASSERT(ringbuf_free_space(&rb) == (sizeof(buf) - 1));
	ASSERT(ringbuf_write(&rb, tmp, sizeof(tmp)) == sizeof(tmp));
	ASSERT(ringbuf_len(&rb) == sizeof(tmp));
	ASSERT(ringbuf_free_space(&rb) == ((sizeof(buf) - 1) - sizeof(tmp)));
	ASSERT(ringbuf_read(&rb, tmp2, sizeof(tmp2)) == sizeof(tmp2));
	ASSERT(array_cmp(tmp, tmp2, sizeof(tmp)));
	ASSERT(ringbuf_free_space(&rb) == (sizeof(buf) - 1));
	ASSERT(ringbuf_len(&rb) == 0);
	ASSERT(ringbuf_write(&rb, big, sizeof(big)) == (sizeof(buf) - 1));
	ASSERT(ringbuf_read(&rb, big, sizeof(big)) == (sizeof(buf) - 1));
}

int cmd_test(cmd_ctx_t *ctx, const char *args)
{
	if (args == NULL) {
		return 3;
	}
	ctx->printf("hello from test\n");

	if (strcmp(args, "argstest")) {
		return 1;
	} else {
		return 2;
	}
}

static bool test_cmd_pass = false;
int	    test_cmd_helper(const char *__restrict __fmt, ...)
{
	if (!strcmp(__fmt, "hello from test\n")) {
		test_cmd_pass = true;
	}
	return 0;
}

void test_cmd(void)
{
	cmd_ctx_t cmd_ctx = {test_cmd_helper};
	cmd_t	  cmd	  = {"test", &cmd_test};
	ASSERT(cmd_parse("test blabla", &cmd_ctx, &cmd, 1) == 1);
	ASSERT(cmd_parse("test argstest", &cmd_ctx, &cmd, 1) == 2);
	ASSERT(cmd_parse("test", &cmd_ctx, &cmd, 1) == 3);
	ASSERT(test_cmd_pass == true);
}

int main(int argc, char **argv)
{
	(void)argc;
	(void)argv;

	test_ringbuf();
	test_linbuf();
	test_cmd();
	printf("%i failed, %i passed and %i tests total", failed, tests - failed, tests);
	return failed;
}
